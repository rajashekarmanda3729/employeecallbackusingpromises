const {readAFile,retriveDataForIds,groupDataBasedOnCompanies,
    getDataFromCompnay,removeDetailsOfId,sortData,swapElementPositions,
    addNewKeyAndValueToData} = require('./solutionEmployeeCallBack.cjs')


readAFile('data.json').then((result) =>{
    return retriveDataForIds([2,13,28],result)
}).then((result) =>{
    return groupDataBasedOnCompanies(result)
}).then((result) =>{
    return getDataFromCompnay(result)
}).then((result) =>{
    return removeDetailsOfId(2,result)
}).then((result) =>{
    return sortData(result)
}).then((result) =>{
    return swapElementPositions(result)
}).then((result) =>{
   return addNewKeyAndValueToData(result)
}).then((result) =>{
    console.log(result)
}).catch((err) =>{
    console.log(err)
})